@extends('gallery')

@section('content')
<div class="row">
	<div class="col-md-10">
		<h1>{{ $gallery->name }}</h1>
		<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseDetails" aria-expanded="false" aria-controls="collapseDetails">
			Gallery Details
		</button>
		<br>
		<br>
		<div class="collapse" id="collapseDetails">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>ID:</dt>
					<dd>{{ $gallery->id }}</dd>

					<dt>Name:</dt>
					<dd>{{ $gallery->name }}</dd>

					<dt>Owner:</dt>
					<dd>{{ $gallery->owner->first_name.' '.$gallery->owner->last_name}} (<a href="mailto:{{$gallery->owner->email}}">{{$gallery->owner->email}}</a>)</dd>

					<dt>Created:</dt>
					<dd>{{ $gallery->created_at }}</dd>

					<dt>Updated:</dt>
					<dd>{{ $gallery->updated_at }}</dd>
				</dl>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2"><h3>Step 1 - Enter your info</h3></div>
	<div class="col-md-10" >
		<label for="guestName">Name:</label>
		<input id="guestName" name="guestName" type="text" size="20" class="form-control" value="{{$_GET['guestName'] or null}}">
		<label for="guestPhone">Phone</label>
		<input id="guestPhone" name="guestPhone" type="text"  class="form-control bfh-phone" data-format="+1 (ddd) ddd-dddd" size="40" value="{{$_GET['guestPhone'] or null}}">
		<small class="text-muted">We'll never share your phone with anyone else.</small>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-2"><h3>Step 2 - Upload Photos</h3></div>
	<div class="col-md-10" style="display: flex;  flex-direction: column;  justify-content: center;" >
		<!-- The element where Fine Uploader will exist. -->
		<div id="fine-uploader">
		</div>

		<!-- Fine Uploader -->
		<script src="/packages/fine-uploader/fine-uploader.min.js" type="text/javascript"></script>

		<script type="text/template" id="qq-template">
			<div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
				<div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
					<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
				</div>
				<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
					<span class="qq-upload-drop-area-text-selector"></span>
				</div>
				<div class="qq-upload-button-selector qq-upload-button">
					<div>Select photos (no videos)</div>
				</div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
				<ul class="qq-upload-list-selector qq-upload-list qq-hide" aria-live="polite" aria-relevant="additions removals">
					<li>
						<div class="qq-progress-bar-container-selector">
							<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
						</div>
						<span class="qq-upload-spinner-selector qq-upload-spinner"></span>
						<img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
						<span class="qq-upload-file-selector qq-upload-file"></span>
						<span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
						<input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
						<span class="qq-upload-size-selector qq-upload-size"></span>
						<button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
						<button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
						<button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
						<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
					</li>
				</ul>

				<dialog class="qq-alert-dialog-selector">
					<div class="qq-dialog-message-selector"></div>
					<div class="qq-dialog-buttons">
						<button type="button" class="qq-cancel-button-selector">Close</button>
					</div>
				</dialog>

				<dialog class="qq-confirm-dialog-selector">
					<div class="qq-dialog-message-selector"></div>
					<div class="qq-dialog-buttons">
						<button type="button" class="qq-cancel-button-selector">No</button>
						<button type="button" class="qq-ok-button-selector">Yes</button>
					</div>
				</dialog>

				<dialog class="qq-prompt-dialog-selector">
					<div class="qq-dialog-message-selector"></div>
					<input type="text">
					<div class="qq-dialog-buttons">
						<button type="button" class="qq-cancel-button-selector">Cancel</button>
						<button type="button" class="qq-ok-button-selector">Ok</button>
					</div>
				</dialog>
			</div>
		</script>

	</div>
</div>
<div class="row">
	<div class="col-md-2"><h3>Step 3 - Download Photos</h3></div>
	<div class="col-md-10">
		<div id="accordion" class="panel-group">
			@foreach( $guests as $guest )
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#{{str_replace(' ','',$guest->name)}}">{{ $guest->name }}</a>
						</h4>
					</div>
					<div id="{{str_replace(' ','',$guest->name)}}" class="panel-collapse collapse">
						<div class="panel-body">
							<p><a href="sms:{{$guest->phone}}" class="button">Contact {{ $guest->name }}</a></p>
							<p>
								@if(count($photos) > 0 )
									@foreach( $photos as $photo )
										@if( $photo->guest_id == $guest->id && $photo->photo_type_id == 1 )
											<a href="{{$photo->largePhoto()->file_path or null}}"><img src="{{$photo->file_path or null}}"></a>
										@endif
									@endforeach
								@endif
							</p>
						</div>
					</div>
				</div>
			@endforeach

		</div>





	</div>

</div>

<script>
	$(document).ready(function(){
		$("#guestPhone").mask("(999) 999-9999");

		function check_done() {
			// Alert the user when all uploads are completed.
			// You probably want to execute a different action though.
			if (allUploadsCompleted() === true) {
				location.reload();
			}
		}

		function allUploadsCompleted() {
			// If and only if all of Fine Uploader's uploads are in a state of
			// completion will this function fire the provided callback.

			// If there are 0 uploads in progress...
			if (uploader.getInProgress() === 0) {
				var failedUploads = uploader.getUploads({ status: qq.status.UPLOAD_FAILED });
				// ... and none have failed
				if (failedUploads.length === 0) {
					// They all have completed.
					return true;
				}
			}
			return false;
		}



		var uploader = new qq.FineUploader({
			debug: true,
			element: document.getElementById('fine-uploader'),
			request: {
				endpoint: '/galleries/' + "{{ $gallery->slug }}"
				, params: {
					guestName: $('#guestName').val()
					, guestPhone: $('#guestPhone').val()
				}
			},
			workarounds:{
				iosEmptyVideos:false
			},
			multiple: true,
			text: {
				uploadButton: '<i class="icon-plus icon-white">Select Files</i> '
			},
			deleteFile: {
				enabled: false,
				endpoint: '/galleries/' + "{{ $gallery->slug }}"
			},
			retry: {
				enableAuto: true
			},
			scaling: {
				sendOriginal: false,
				includeExif: false,

				sizes: [
					{name: "thumbnail", maxSize: 100}
					, {name: "large", maxSize: 1024}
				]
			},
			callbacks: {
				onStatusChange: function (id, oldStatus, newStatus) {
					// This will check to see if a file that has been cancelled
					// would equate to all uploads being 'completed'.
					if (newStatus === qq.status.CANCELLED) {
						check_done();
					}
				},
				onComplete: check_done
			}

		});
	});
</script>
@endsection