@extends('app')

@section('content')
	<div class="row">
		<div class="col-md-4 col-md-offset-4" style="text-align:center;">
			<h1>New Gallery</h1>
		</div>
	</div>
	{!! Form::open(array('action'=>"GalleriesController@store")) !!}
	@include('galleries.form')
	<button id="submit" class="btn btn-default">Create</button>
	{!! Form::close() !!}

@stop