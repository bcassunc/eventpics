@extends('app')

@section('content')

<div class="row">
	<div class="col-md-4 col-md-offset-4" style="text-align:center">
		<h1>Galleries</h1>
	</div>
	<div class="col-md-10 col-md-offset-1">
		<div style="text-align:right;">
			<a href='/galleries/create' class="btn btn-default">New Gallery</a>
		</div>
		<br />
		<table class="table table-bordered table-striped" id="galleriestable">
			<thead>
			<tr>
				<th>Name</th>
				<th>Owner</th>
				<th>Created</th>
				<th>Updated</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($galleries as $gallery)
				<tr>
					<td><a href="/galleries/{{ $gallery->slug }}">{{ $gallery->name }}</a></td>
					<td>{{ $gallery->owner->first_name.' '.$gallery->owner->last_name }}</td>
					<td>{{ $gallery->created_at }}</td>
					<td>{{ $gallery->updated_at }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<br />
	</div>
</div>
    <script type="text/javascript">
    		$('#galleriestable').DataTable();
    </script>

@endsection