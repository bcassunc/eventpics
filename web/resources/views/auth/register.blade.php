@extends('app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<!-- <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">-->
					{!! Form::open(
    array(
        'method' => 'auth@register', 
        'class' => 'form', 
        'files' => true)) !!}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
								<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="address" value="{{ old('address') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">City/State/Zip</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="city" value="{{ old('city') }}" placeholder="City">
								<input type="text" class="form-control" name="state" value="{{ old('state') }}" placeholder="State">
								<input type="text" class="form-control" name="zip" value="{{ old('zip') }}" placeholder="Zip">

							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="title" value="{{ old('title') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Office Location</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="office_location" value="{{ old('office_location') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Work Days</label>
							<div class="col-md-6">
								<label><input type="checkbox" class="form-control" name="work_monday" {{ old('work_monday') == "on" ? 'checked=""' : '' }} />Monday</label>
								<label><input type="checkbox" class="form-control" name="work_tuesday" {{ old('work_tuesday') == "on" ? 'checked=""' : '' }} />Tuesday</label>
								<label><input type="checkbox" class="form-control" name="work_wednesday" {{ old('work_wednesday') == "on" ? 'checked=""' : '' }} />Wednesday</label>
								<label><input type="checkbox" class="form-control" name="work_thursday" {{ old('work_thursday') == "on" ? 'checked=""' : '' }} />Thursday</label>
								<label><input type="checkbox" class="form-control" name="work_friday" {{ old('work_friday') == "on" ? 'checked=""' : '' }} />Friday</label>
								<label><input type="checkbox" class="form-control" name="work_saturday" {{ old('work_saturday') == "on" ? 'checked=""' : '' }} />Saturday</label>
								<label><input type="checkbox" class="form-control" name="work_sunday" {{ old('work_sunday') == "on" ? 'checked=""' : '' }} />Sunday</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Work Hours</label>
							<div class="col-md-6">
								
									<input type="text" class="form-control" name="workhours_start" id="workhours_start" value="{{ old('workhours_start') }}" />
								to
									<input type="text" class="form-control" name="workhours_end" id="workhours_end" value="{{ old('workhours_end') }}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Phone</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="555-555-5555">
								<select class="form-control" name="channel_name" value="{{ old('channel_name') }}">
									<option value="">...</option>
									<option value="Cell">Cell</option>
									<option value="Home">Home</option>
									<option value="Work">Work</option>
								</select>
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label">Resume Upload</label>
							<div class="col-md-6">
								{!! Form::file('resume_upload','',array('id'=>'resume_upload','class'=>'form-control')) !!}
							</div>
						</div>

						

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function setTimePickers() {
		$('#workhours_start').timepicker( { timeFormat: 'HH:mm:ss', } );
		$('#workhours_end').timepicker( { timeFormat: 'HH:mm:ss', } );
	}

	window.addEventListener("load", setTimePickers, false);
</script>
@endsection
