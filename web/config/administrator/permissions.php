<?php
/**
 * Actors model config
 */
return [
  'title' => 'Permissions',
  'single' => 'permission',
  'model' => 'Bican\Roles\Models\Permission',
  /**
   * The display columns
   */
  'columns' => [
    'id',
    'name' => [
      'title' => 'Name'
    ],
    'slug' => [
      'title'=>'Slug'
    ],
    'model'=>[
      'title'=>'Model'
    ]
  ],
  /**
   * The filter set
   */
  'filters' => [
    'id',
    'name' => [
      'title' => 'Name',
    ],
    'slug' => [
      'title' => 'Slug',
    ],
    'description'=>[
      'title'=>'Description'
    ],
    'roles' => [
      'title'=>'Roles',
      'type'=>'relationship',
      'name_field'=>'name',
    ]
  ],
  /**
   * The editable fields
   */
  'edit_fields' => [
    'name' => [
      'title' => 'Name',
    ],
    'slug' => [
      'title' => 'slug',
    ],
    'description' => [
      'title' => 'Description',
    ],
    'model'=> [
    ],
    'roles' => [
      'title'=>'Roles',
      'type'=>'relationship',
      'name_field'=>'name',
    ],
  ],

  /**
   * Action permissions
   */
  'permission' => function(){
    return Auth::user()->level() > 9;
  },
  'action_permissions' => [
    'create' => function($model){
      return Auth::user()->can('create.permissions');
    },
    'edit' => function($model){
      return Auth::user()->can('update.permissions');
    },
    'delete' => function($model){
      return Auth::user()->can('delete.permissions');
    }
  ],
];