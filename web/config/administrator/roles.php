<?php
/**
 * Actors model config
 */
return array(
  'title' => 'Roles',
  'single' => 'role',
  'model' => 'Bican\Roles\Models\Role',
  'sort' => [
    'field'=>'level',
    'direction'=>'desc'
  ],
  /**
   * The display columns
   */
  'columns' => array(
    'id',
    'name' => array(
      'title' => 'Name',
    ),
    'slug' => array(
      'title'=>'Slug',
    ),
    'level' => [
      'title'=>'Level'
    ]
  ),
  /**
   * The filter set
   */
  'filters' => array(
    'id',
    'name' => array(
      'title' => 'Name',
    ),
    'slug' => array(
      'title' => 'Slug',
    ),
    'permissions' => array(
      'title'=>'Permissions',
      'type'=>'relationship',
      'name_field'=>'name',
    )
  ),
  /**
   * The editable fields
   */
  'edit_fields' => array(
    'name' => array(
      'title' => 'Name',
    ),
    'slug' => array(
      'title' => 'slug',
    ),
    'description' => array(
      'title' => 'Description',
    ),
    'level' => array(
      'title'=>'level',
      'type'=>'number'
    ),
    'permissions' => array(
      'title'=>'Permissions',
      'type'=>'relationship',
      'name_field'=>'name',
    )
  ),

  /**
   * Action permissions
   */
  'permission' => function(){
    return Auth::user()->level() > 9;
  },
  'action_permissions' => [
    'create' => function($model){
      return Auth::user()->can('create.roles');
    },
    'update' => function($model){
      return Auth::user()->can('update.roles');
    },
    'delete' => function($model){
      return Auth::user()->can('delete.roles');
    }
  ],
);