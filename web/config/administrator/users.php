<?php
/**
 * Actors model config
 */
return [
  'title' => 'Users',
  'single' => 'user',
  'model' => 'App\User',
  'sort' => [
    'field'=>'full_name',
    'direction'=>'asc'
  ],
  /**
   * The display columns
   */
  'columns' => [
    'id',
    'full_name' => [
      'title' => 'Name',
      'select' => "CONCAT((:table).first_name, ' ', (:table).last_name)",
    ],
    'email' => [
      'title'=>'Email',
    ],
  ],
  /**
   * The filter set
   */
  'filters' => [
    'id',
    'first_name' => [
      'title' => 'First Name',
    ],
    'last_name' => [
      'title' => 'Last Name',
    ],
    'email' => [
      'title' => 'Email',
      'type' => 'text'
    ],

  ],
  /**
   * The editable fields
   */
  'edit_fields' => [
    'first_name' => [
      'title' => 'First Name',
      'type' => 'text',
    ],
    'last_name' => [
      'title' => 'Last Name',
      'type' => 'text',
    ],
    'email' => [
      'title' => 'Email',
      'type' => 'text',
    ],
    'password' => [
      'title' => 'Password',
      'type' => 'password',
    ],
    'roles' => [
      'title' => 'Roles',
      'type'=>'relationship',
      'name_field'=>'name'
    ],
    'userPermissions' => [
      'title' =>'User Permissions',
      'type'=>'relationship',
      'name_field'=>'name',
    ],
  ],

  /**
   * Action permissions
   */
  'permission' => function(){
    return Auth::user()->level() > 9;
  },
  'action_permissions' => [
    'create' => function($model){
      return Auth::user()->can('create.users');
    },
    'edit' => function($model){
      return Auth::user()->can('update.users');
    },
    'delete' => function($model){
      return Auth::user()->can('delete.users');
    }
  ],
];