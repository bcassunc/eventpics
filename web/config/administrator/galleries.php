<?php
/**
 * Actors model config
 */
return array(
  'title' => 'Galleries',
  'single' => 'gallery',
  'model' => 'App\Gallery',
  /**
   * The display columns
   */
  'columns' => array(
    'id',
    'name' => array(
      'title' => 'Name'
    ),
    'slug',
    'created_at' => [
      'title' => 'Created'
    ],
    'updated_at' => [
      'title' => 'Updated'
    ]
  ),
  /**
   * The filter set
   */
  'filters' => array(
    'id',
    'name' => array(
      'title' => 'Name',
    ),
    'slug'
  ),
  /**
   * The editable fields
   */
  'edit_fields' => array(
    'name' => array(
      'title' => 'Name',
    ),
  ),

  /**
   * Action permissions
   */
  'permission' => function(){
    return Auth::user()->level() > 9;
  },
  // 'action_permissions' => [
  //   'create' => function($model){
  //     return Auth::user()->can('create.areas');
  //   },
  //   'update' => function($model){
  //     return Auth::user()->can('update.areas');
  //   },
  //   'delete' => function($model){
  //     return Auth::user()->can('delete.areas');
  //   }
  // ],
);