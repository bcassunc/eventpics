<?php

use Illuminate\Database\Seeder;
use App\PhotoType;

class PhotoTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PhotoType::create([
            'id' => 1
            ,'name' =>'thumbnail'
        ]);

        PhotoType::create([
            'id' => 2
            ,'name' =>'scaled image'
        ]);
    }
}
