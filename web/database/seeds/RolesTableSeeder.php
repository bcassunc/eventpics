<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class RolesTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Model::unguard();

    $progRole = Role::updateOrCreate([
      'id'=>'1',
      'name'=>'Programmer',
      'slug'=>'programmer',
      'description'=>'Programmer working on the system.  Has all privileges.',
      'level'=>100,
    ]);

    // add permissions to programmer roles
    if( $permissions = Permission::all() ){
      foreach( $permissions as $perm ){
        $progRole->attachPermission($perm);
      }    
    }

  }

}
