<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Bican\Roles\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Model::unguard();

        // Users permissions
        Permission::create([
        'name'=>'Create users',
        'slug'=>'create.users'
        ]);
        Permission::create([
        'name'=>'Edit users',
        'slug'=>'edit.users'
        ]);
        Permission::create([
        'name'=>'Delete users',
        'slug'=>'delete.users'
        ]);

        // role/permissions permissions
        Permission::create([
            'name'=>'Create roles',
            'slug'=>'create.roles'
        ]);
        Permission::create([
            'name'=>'Edit roles',
            'slug'=>'edit.roles'
        ]);
        Permission::create([
            'name'=>'Delete roles',
            'slug'=>'delete.roles'
        ]);

        Permission::create([
            'name'=>'Create Permissions',
            'slug'=>'create.permissions'
        ]);
        Permission::create([
            'name'=>'Edit Permissions',
            'slug'=>'edit.permissions'
        ]);
        Permission::create([
            'name'=>'Delete Permissions',
            'slug'=>'delete.permissions'
        ]);

    }
}
