<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Upload;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Auth;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // $uploads = Upload::where('user_id', Auth::user()->id)->get();
 
        $uploads = Auth::user()->uploads;

        return view('uploads.uploadform', compact('uploads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            // 'name' => 'required|unique:uploads|max:255',
            'name' => 'required|max:255',
            'upload' => 'required'
        ]);

        // get ready to use store a file
        $file = $request->file('upload');
        $mime = $file->getClientMimeType();
        $extension = $file->getClientOriginalExtension();
        // $storage_name = $file->getFilename().'.'.$extension;
        $storage_name = uniqid();

        // store the file
        Storage::disk('local')->put(
            $storage_name,
            File::get($file)
        );
        // dd( $storage_name );

        // save the upload
        $upload = new Upload(array(
            'name' => $request->get('name'),
            'file_path' => $storage_name,
            'mime' => $mime,
            'user_id' => Auth::user()->id,
        ));
        $upload->save();

        return redirect('upload');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $upload = Upload::where('file_path', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get( $id );
        return (new Response($file, 200))->header('Content-Type', $upload->mime);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $upload = Upload::where('file_path', '=', $id)->delete();
        return redirect('upload');
    }
}
