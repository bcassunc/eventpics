<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Gallery;
use App\Photo;
use App\Guest;
use App\User;
use Auth;
use Exception;
use Log;
use Response;

class GalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::all();
        return view ('galleries.list', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$params = $request->all();

	    if( Gallery::where('name',$params['name'])->count() > 0 ){
		    throw new Exception('Name already exists');
	    }
		$gallery = Gallery::create($params);

	    return redirect('galleries/'.$gallery->slug);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $gallery = Gallery::where('slug',$slug)->get()->first();
        $photos = $gallery->photos;

	    $guest_ids = [];
	    foreach($photos as $photo){
		    if( !in_array($photo->guest_id,$guest_ids) ){
			    $guest_ids[] = $photo->guest_id;
		    }
	    }
	    $guests = Guest::whereIn('id',$guest_ids)->get();

        return view('galleries.detail', compact('gallery','photos','guests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Upload photos for the specified gallery
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadPhotos( $slug, Request $request )
    {
	    try{
	        $params = $request->all();

		    $gallery = Gallery::where('slug',$slug)->first();

		    // Get user_id or guest_id if exists
		    $guest_id = NULL;

		    if( $params['guestPhone'] == '' || $params['guestName'] == ''){
			    throw new Exception('Name and phone must be entered before uploading files');
		    }

		    // get guest info
		    $guest = Guest::firstOrCreate([
			    'phone'=>$params['guestPhone']
		    ]);
		    $guest->name = $params['guestName'];
		    $guest->save();
		    $guest_id = $guest->id;


	        foreach($request->files as $file ){
		        if( strpos($params['qqfilename'],'thumbnail') > 0 ){
			        $PhotoType = 1;
		        }else{
			        $PhotoType = 2;
		        }

		        $file->move("./",$params['qquuid'].'-'.$params['qqfilename']);
	            Photo::updateOrCreate([
		            'gallery_id'=>$gallery->id
		            ,'guest_id'=>$guest_id
		            ,'mime'=>'jpg'
		            ,'file_path'=>'/'.$params['qquuid'].'-'.$params['qqfilename']
		            ,'photo_type_id' => $PhotoType
		            ,'parentuuid'=>$params['qqparentuuid']
		            ,'uuid'=>$params['qquuid']
	            ]);
	        }
		    return Response::json(['success'=>true]);

	    }catch( Exception $e ){
			return Response::json(['success'=>false,'error'=>$e]);
		}
    }

	/**
	 * Delete photo associated with UUID
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function deletePhoto( $uuid, Request $request ){
		try{
			$photo = Photo::where('uuid',$uuid)->first();
			$photos = Photo::where('parentuuid', $photo->parentuuid)->get();
			foreach($photos as $iphoto){
				$iphoto->delete();
			}
			return Response::json(['success'=>true]);
		}catch( Exception $e ){
			return Response::json(['success'=>false]);
		}
	}

}
