<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// protected routes
Route::group(['middleware'=>'auth'], function(){
    Route::get('/', function () {
        return redirect('/galleries/');
    });

    Route::get('/home', function () {
        return redirect('/galleries/');
    });


    // log viewer route
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::post('galleries/{slug}', 'GalleriesController@uploadPhotos');
Route::delete('galleries/{slug}/{uuid}', 'GalleriesController@deletePhoto');

Route::resource('galleries', 'GalleriesController');

