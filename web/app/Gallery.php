<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Auth;

class Gallery extends Model implements SluggableInterface{
	use SluggableTrait;

    protected $fillable = ['name','owner_id','slug'];

	/**
	 * Gallery constructor.
	 * @param array $attributes
	 */
	public function __construct( array $attributes = [] )
	{
		parent::__construct($attributes);

		if( !isset($this->owner_id) && Auth::user() )
		{
			$this->owner_id = Auth::user()->id;
		}

	}

	public function owner(){
		return $this->belongsTo('App\User');
	}

	public function photos()
	{
		return $this->hasMany('App\Photo');
	}


}
