<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Photo extends Model {

	protected $guarded = ['id'];

	public function gallery(){
		return $this->belongsTo('App\Gallery');
	}

	public function photoType(){
		return $this->belongsTo('App\PhotoType');
	}

	public function largePhoto(){
		return Photo::where('parentuuid',$this->parentuuid)->where('photo_type_id',2)->first();
	}
}
