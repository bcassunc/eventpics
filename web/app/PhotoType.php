<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoType extends Model
{
    protected $fillable = ['id','name'];

	public function photos(){
		return $this->hasMany('App\Photo');
	}
}
