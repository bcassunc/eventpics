<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Hash;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission;

        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name','last_name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * hashes the password value before adding it to $this->attributes
     *
     * @param string $value
     * @return void
     * @author TJ Ward
     **/
    public function setPasswordAttribute($value)
    {
        if( Hash::needsRehash($value) ){
            $this->attributes['password'] = Hash::make($value);
        }else{
            $this->attributes['password'] = $value;
        }
    }

    public function fullName(){
        return $this->first_name . " " . $this->last_name;
    }

    public function getFullNameAttribute(){
        return $this->first_name . " " . $this->last_name;
    }

    public function galleries()
    {
        return $this->hasMany('App\Gallery');
    }
}
